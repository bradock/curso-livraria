<?php

    include('banco.php');

    $id = $_GET['id'];

    $queryIdTipo = "SELECT * FROM livros WHERE id_livro = $id";

    try{
        $resul = mysqli_query($conexao,$queryIdTipo);
    }catch(Exception $e){
        echo $e;
    }
    
?>

<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="estilo.css">
        <title>Livros</title>

    </head>
    <body>
        <nav>
            <ul class="menu">
                    <li><a href="index.php">Livros</a></li>
                    <li><a href="formulario_add.php">Adicionar Livros</a></li>               
            </ul>
        </nav>
        
        <h2>Editar Livros</h2>
        <form action="editando.php" method="POST">
            <?php if(mysqli_num_rows($resul) > 0){
                    while($linha = mysqli_fetch_assoc($resul)){ ?>
                
            <label for="ParaNome">Nome:</label><br>
            <input type="text" id="ParaNome" value="<?=$linha['nome_livro']?>" name="nome"><br>
            <label for="ParaDescricao">Descrição</label><br>
            <input type="text" id="ParaDescricao" value="<?=$linha['descricao_livro']?>" name="descricao"><br>
            <label for="ParaCensura">Censura Idade</label><br>
            <input type="number" id="ParaCensura" value="<?=$linha['censura_idade']?>" name="censura"><br>
            <label for="ParaStatus">Status</label><br>
            <input type="text" id="ParaStatus" value="<?=$linha['status']?>" name="status"><br><br>
            <input type="hidden" name="id" value="<?=$id?>">
            <input type="submit" class="btn" value="Editar Livro">

            <?php }
                }?>
        </form> 
    </body>
</html>

