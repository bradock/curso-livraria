<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="estilo.css">
        <title>Livros</title>

    </head>
    <body>
        <nav>
            <ul class="menu">
                    <li><a href="index.php">Livros</a></li>
                    <li><a href="formulario_add.php">Adicionar Livros</a></li>               
            </ul>
        </nav>
        <h2>Cadastrar Livros</h2>
        <form action="cadastro.php" method="POST">
            <label for="ParaNome">Nome:</label><br>
            <input type="text" id="ParaNome" name="nome"><br>
            <label for="ParaDescricao">Descrição</label><br>
            <input type="text" id="ParaDescricao" name="descricao"><br>
            <label for="ParaCensura">Censura Idade</label><br>
            <input type="number" id="ParaCensura" name="censura"><br>
            <label for="ParaStatus">Status</label><br>
            <input type="text" id="ParaStatus" name="status"><br><br>
            <input type="submit" class="btn" value="Cadastrar Livro">
        </form> 
    </body>
</html>




