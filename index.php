<?php

include('banco.php');

$queryIdTipo = "SELECT * FROM livros";

try{
    $resul = mysqli_query($conexao,$queryIdTipo);
}catch(Exception $e){
    echo $e;
}

?>

<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="estilo.css">
        <title>Livros</title>

    </head>
    <body>
        <nav>
            <ul class="menu">
                    <li><a href="index.php">Livros</a></li>
                    <li><a href="formulario_add.php">Adicionar Livros</a></li>               
            </ul>
        </nav>
        <h2>Livros</h2>
        <table>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nome</th>
                    <th>Descrição</th>
                    <th>Data</th>
                    <th>Censura Idade</th>
                    <th>Status</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
                <?php if(mysqli_num_rows($resul) > 0){

                    while($linha = mysqli_fetch_assoc($resul)){?>

                    <tr>
                        <td><?=$linha['id_livro']?></td>
                        <td><?=$linha['nome_livro']?></td>
                        <td><?=$linha['descricao_livro']?></td>
                        <td><?=$linha['data_cadastro']?></td>
                        <td><?=$linha['censura_idade']?></td>
                        <td><?=$linha['status']?></td>
                        <td><a href="editar.php?id=<?=$linha['id_livro']?>">Editar</a></td>
                    </tr>   
                    <?php }

                }else{ ?>
                    <tr>
                        <td>
                            <?="Nenhum ID";?>
                        </td>
                        <td>
                            <?="Nenhum Nome";?>
                        </td>
                        <td>
                            <?="Nenhuma Descrição";?>
                        </td>
                        <td>
                            <?="Nenhuma Data";?>
                        </td>
                        <td>
                            <?="Nenhuma Censura";?>
                        </td>
                        <td>
                            <?="Nenhum Status";?>
                        </td>
                        <td>
                            <?="Nenhuma Ação";?>
                        </td>
                    </tr>
                <?php }

                ?>
            </tbody>
        </table>
        <hr><br>
    </body>
</html>

<?php
    $queryContador = "SELECT count(*) FROM livros";

    try{
        $resultadoContador = mysqli_query($conexao,$queryContador);
    }catch(Exception $e){
        echo $e;
    }

    if(mysqli_num_rows($resultadoContador) > 0){

        while($linha = mysqli_fetch_assoc($resultadoContador)){?>
            <div class="box">
                <h2 class="resultado"><?php echo $linha["count(*)"]?> Livros Cadastrados</h2>
            </div>
       <?php }
    }
?>